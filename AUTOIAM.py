#!/usr/bin/python
#Automation of IAM software

#Introduction to program
print ("Welcome to AUTOIAM where we will completely configure your system to correlate your company's departments, employee logins, and security measures. Enter your company name.")
CORP_NAME = input()

#Begin collecting data; Departments
print ("Welcome "+CORP_NAME+", Begin with entering each department in your business.")
USER_INP = input()

#Funtion to automate list using loop
LIST_NAME = list()
def LIST_ADD (USER_INP):
    LIST_NAME.clear
    while (USER_INP != "0"):
        LIST_NAME.append(USER_INP)
        print("Enter Next Department or 0 to complete.")
        USER_INP = input()
LIST_ADD(USER_INP)
DEP_LIST = LIST_NAME

#Collecting data; Employees
print("Enter Employee")
USER_INP = input()

#Function to create list
EMP_DEP_LIST = list()
EMP_LIST = list()
def DATAB_LIST (USER_INP):
    while (USER_INP != "0"):
        EMP_LIST.append (USER_INP)
        print("Which department is " + USER_INP + " in?")
        print(*DEP_LIST)
        USER_INP = input()
        EMP_DEP_LIST.append (USER_INP)

        print("Enter Next Employee or 0 to complete.")
        USER_INP = input()

DATAB_LIST(USER_INP)
#Create Groups with list
import os

def ADD_GROUPS_LINX (LISTN):
    for item in LISTN:
        os.system('groupadd '+ item)

ADD_GROUPS_LINX (DEP_LIST)

#Create Users
import random
import string

def RAND_PASS_GEN():
    CHARS = string.ascii_letters + string.punctuation
    return ''.join(random.choice(CHARS) for x in range (10))


def ADD_USER_GROUP(EMPL, DEPL):
	x=0
	for item in EMPL:
		os.system('adduser ' + item)
		os.system('usermod -p '+ RAND_PASS_GEN() + ' '  + item)
		os.system('usermod -g ' + DEPL[x] + ' '  + item)
		x+=1

ADD_USER_GROUP(EMP_LIST, EMP_DEP_LIST)
