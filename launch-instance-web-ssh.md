# Set the Region

    AZ=`curl -s http://169.254.169.254/latest/meta-data/placement/availability-zone`
    export AWS_DEFAULT_REGION=${AZ::-1}

# Obtain latest Linux AMI

    AMI=$(aws ssm get-parameters --names /aws/service/ami-amazon-linux-latest/amzn2-ami-hvm-x86_64-gp2 --query 'Parameters[0].[Value]' --output text)
    echo $AMI

## This command did the following:
    Obtained the Region where the instance is running

    Called the AWS Systems Manager (ssm) and used the get-parameters command to retrieve a value from the Parameter Store
    The AMI requested was for Amazon Linux 2 (amzn2-ami)

    The AMI ID has been stored in an Environment Variable called AMI

    If your SSH session disconnects, it will lose the information stored in environment variables. 
    Once you reconnect, you will need to re-run all of the steps in this task, starting with the above commands to obtain the AMI ID.

# This uses the AWS CLI to retrieve the Subnet ID of the subnet named Public Subnet.

    SUBNET=$(aws ec2 describe-subnets --filters 'Name=tag:Name,Values=Public Subnet' --query Subnets[].SubnetId --output text)
    echo $SUBNET

# The command retrieves the Security Group ID of the Web Security Group.

    SG=$(aws ec2 describe-security-groups --filters Name=group-name,Values=WebSecurityGroup --query SecurityGroups[].GroupId --output text)
    echo $SG

# Paste this command to download the User Data script:

    wget https://aws-tc-largeobjects.s3.us-west-2.amazonaws.com/CUR-TF-100-RESTRT-1/171-lab-JAWS-create-ec2/s3/UserData.txt

    cat UserData.txt

## The script does the following:

    Installs a web server
    Downloads a zip file containing the web application
    Installs the web application

# Launch instance

    INSTANCE=$(\
    aws ec2 run-instances \
    --image-id $AMI \
    --subnet-id $SUBNET \
    --security-group-ids $SG \
    --user-data file:///home/ec2-user/UserData.txt \
    --instance-type t3.micro \
    --tag-specifications 'ResourceType=instance,Tags=[{Key=Name,Value=Web Server}]' \
    --query 'Instances[*].InstanceId' \
    --output text \
    )
    echo $INSTANCE

## The command launches a new instance (run_instances) using these parameters:

    Image: Uses the AMI value obtained earlier from the Parameter Store
    
    Subnet: Specifies the Public Subnet obtained earlier and, by association, the VPC in which to launch the instance

    Security Group: Uses the Web Security Group obtained earlier, which permits HTTP access
    
    User Data: References the User Data script you downloaded, which installs the web application
    
    Instance Type: Specifies the type of instance to launch
    
    Tags: Assigns a Name tag with the value of Web Server
    
    The query parameter specifies that the command should return the Instance ID once the instance is launched.
 
    The output parameter specifies that the output of the command should be in text. Other output options are json and table.

    The ID of the new instance has been stored in the INSTANCE environment variable.

# You can monitor the status of the instance via the Management Console, but you can also query the status via the AWS CLI.

    aws ec2 describe-instances --instance-ids $INSTANCE --query 'Reservations[].Instances[].State.Name' --output text